package configmodels

import (
	"errors"

	stringutil "go.jamie.mt/toolbox/string_util"
)

type S3 struct {
	Name      string `json:"name" mapstructure:"name"`
	Endpoint  string `json:"endpoint" mapstructure:"endpoint"`
	AccessKey string `json:"accessKey" mapstructure:"access_key"`
	SecretKey string `json:"secretKey" mapstructure:"secret_key"`
	Bucket    string `json:"bucket" mapstructure:"bucket"`
	Region    string `json:"region" mapstructure:"region"`
	UseSSL    bool   `json:"useSSL" mapstructure:"use_ssl"`
}

func (s *S3) Validate() (err error) {
	if stringutil.IsTrimmedEmpty(s.Name) {
		err = errors.Join(err, errors.New("`s3.name` is required"))
	}
	if stringutil.IsTrimmedEmpty(s.Endpoint) {
		err = errors.Join(err, errors.New("`s3.endpoint` is required"))
	}
	if stringutil.IsTrimmedEmpty(s.AccessKey) {
		err = errors.Join(err, errors.New("`s3.access_key` is required"))
	}
	if stringutil.IsTrimmedEmpty(s.SecretKey) {
		err = errors.Join(err, errors.New("`s3.secret_key` is required"))
	}
	if stringutil.IsTrimmedEmpty(s.Bucket) {
		err = errors.Join(err, errors.New("`s3.bucket` is required"))
	}
	if stringutil.IsTrimmedEmpty(s.Region) {
		err = errors.Join(err, errors.New("`s3.region` is required"))
	}
	return err
}
