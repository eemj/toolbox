package configmodels

import (
	"errors"
	"fmt"
	"net"
	"strconv"

	stringutil "go.jamie.mt/toolbox/string_util"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
)

type GRPCServer struct {
	Transports []GRPCServerTransport `json:"transports" mapstructure:"transports"`
}

func (c *GRPCServer) Validate() (err error) {
	if len(c.Transports) == 0 {
		err = errors.Join(err, errors.New("`grpc.transports` is required"))
	}

	for index, transport := range c.Transports {
		if transportErr := transport.Validate(); transportErr != nil {
			err = errors.Join(err, fmt.Errorf("`grpc.transports`@`%d`: %v", index, transportErr))
		}
	}

	return err
}

type GRPCServerTransport struct {
	Host        string                `json:"host" mapstructure:"host"`
	Port        int                   `json:"port" mapstructure:"port"`
	Credentials GRPCServerCredentials `json:"credentials" mapstructure:"credentials"`
}

func (g GRPCServerTransport) HostPort() string {
	return net.JoinHostPort(
		g.Host,
		strconv.FormatUint(uint64(g.Port), 10),
	)
}

func (g *GRPCServerTransport) Validate() (err error) {
	if stringutil.IsTrimmedEmpty(g.Host) {
		g.Host = "0.0.0.0"
	}

	if g.Port < 0 || g.Port > 65535 {
		err = errors.Join(err, errors.New("`grpc.port` must be between 0 and 65535"))
	}

	return errors.Join(err, g.Credentials.Validate())
}

type GRPCServerCredentials struct {
	Secure   bool   `json:"secure" mapstructure:"secure"`
	CertFile string `json:"certFile" mapstructure:"cert_file"`
	KeyFile  string `json:"keyFile" mapstructure:"key_file"`
}

func (c *GRPCServerCredentials) Validate() (err error) {
	if c.Secure {
		if stringutil.IsTrimmedEmpty(c.CertFile) {
			err = errors.Join(err, errors.New("`grpc.credentials.cert_file` is required"))
		}

		if stringutil.IsTrimmedEmpty(c.KeyFile) {
			err = errors.Join(err, errors.New("`grpc.credentials.key_file` is required"))
		}
	}

	return err
}

func (c *GRPCServerCredentials) TransportCredentials() (credentials.TransportCredentials, error) {
	if c.Secure {
		return credentials.NewServerTLSFromFile(
			c.CertFile,
			c.KeyFile,
		)
	}
	return insecure.NewCredentials(), nil
}
