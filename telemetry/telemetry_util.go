package telemetry

import "go.opentelemetry.io/otel/trace"

// WithError attempts to record an error to the span,
// returning the thrown error.
func WithError(span trace.Span, err error) error {
	if err == nil {
		return err
	}
	if span.IsRecording() {
		span.RecordError(err)
	}
	return err
}
