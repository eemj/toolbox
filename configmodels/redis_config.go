package configmodels

import (
	"errors"
	"time"

	"github.com/redis/go-redis/v9"
)

type Redis struct {
	Addresses        []string      `json:"addresses" mapstructure:"addresses"`
	DatabaseNumber   int           `json:"databaseNumber" mapstructure:"database_number"`
	MasterName       string        `json:"masterName" mapstructure:"master_name"`
	SentinelUsername string        `json:"sentinelUsername" mapstructure:"sentinel_username"`
	SentinelPassword string        `json:"sentinelPassword" mapstructure:"sentinel_password"`
	Username         string        `json:"username" mapstructure:"username"`
	Password         string        `json:"password" mapstructure:"password"`
	WriteTimeout     time.Duration `json:"writeDuration" mapstructure:"write_timeout"`
	ReadTimeout      time.Duration `json:"readTimeout" mapstructure:"read_timeout"`
}

func (r *Redis) Validate() (err error) {
	if r.WriteTimeout == 0 {
		r.WriteTimeout = 200 * time.Millisecond
	}
	if r.ReadTimeout == 0 {
		r.ReadTimeout = 200 * time.Millisecond
	}

	if len(r.Addresses) == 0 {
		err = errors.Join(err, errors.New("`redis.addresses` is required"))
	}

	return err
}

func (r *Redis) Instance() redis.UniversalClient {
	return redis.NewUniversalClient(&redis.UniversalOptions{
		Addrs:            r.Addresses,
		DB:               r.DatabaseNumber,
		MasterName:       r.MasterName,
		SentinelUsername: r.SentinelUsername,
		SentinelPassword: r.SentinelPassword,
		Username:         r.Username,
		Password:         r.Password,
		WriteTimeout:     r.WriteTimeout,
		ReadTimeout:      r.ReadTimeout,
	})
}

var _ ConfigValidator = &Redis{}
