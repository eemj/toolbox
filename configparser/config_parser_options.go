package configparser

const (
	DefaultConfigDelimiter      string = "."
	DefaultEnvironmentDelimiter string = "__"
	DefaultFilename             string = "config.json"
)

type ParserOptions struct {
	EnvironmentReplacer  string
	EnvironmentDelimiter string
	ConfigDelimiter      string
	ApplicationName      string
	Filename             string
}
