package configmodels

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	sliceutil "go.jamie.mt/toolbox/slice_util"
	stringutil "go.jamie.mt/toolbox/string_util"
	"go.opentelemetry.io/otel/sdk/trace"
)

type TelemetrySampling string

func (k TelemetrySampling) Valid() bool {
	switch v := strings.ToLower(string(k)); v {
	case "never", "always":
		return true
	default:
		fraction, err := strconv.ParseFloat(v, 64)
		if err != nil {
			return false
		}
		if fraction < 0 || fraction > 1 {
			return false
		}
	}
	return true
}

func (k TelemetrySampling) Sampler() (trace.Sampler, error) {
	switch strings.ToLower(string(k)) {
	case "never":
		return trace.NeverSample(), nil
	case "always":
		return trace.AlwaysSample(), nil
	}
	fraction, err := strconv.ParseFloat(string(k), 64)
	if err != nil {
		return nil, err
	}
	if fraction > 1 {
		return trace.AlwaysSample(), nil
	}
	if fraction < 0 {
		fraction = 0
	}
	return trace.ParentBased(trace.TraceIDRatioBased(fraction)), nil
}

type TelemetryKind string

func (k TelemetryKind) Valid() bool {
	switch k {
	case TelemetryKindZipkin,
		TelemetryKindOTLPGRPC,
		TelemetryKindOTLPHTTP:
		return true
	}
	return false
}

const (
	TelemetryKindZipkin   = "zipkin"
	TelemetryKindOTLPGRPC = "otlp_grpc"
	TelemetryKindOTLPHTTP = "otlp_http"
)

type Telemetry struct {
	Enable   bool              `json:"enable" mapstructure:"enable"`
	Kind     TelemetryKind     `json:"kind" mapstructure:"kind"`
	Sampler  TelemetrySampling `json:"sampler" mapstructure:"sampler"`
	Endpoint string            `json:"endpoint" mapstructure:"endpoint"`
	CertFile *string           `json:"cert_file" mapstructure:"cert_file"`
}

func (c *Telemetry) Validate() (err error) {
	if !c.Enable {
		return nil
	}

	if !c.Sampler.Valid() {
		err = errors.Join(err, fmt.Errorf(
			"`telemetry.sampler` is not valid, expected one of: %v",
			sliceutil.SurroundWith(
				'\'',
				"never",
				"always",
				"`float between 0 and 1`",
			),
		))
	}

	if !c.Kind.Valid() {
		err = errors.Join(err, fmt.Errorf(
			"`telemetry.kind` is not valid, expected one of: %v",
			sliceutil.SurroundWith(
				'\'',
				TelemetryKindZipkin,
				TelemetryKindOTLPGRPC,
				TelemetryKindOTLPHTTP,
			),
		))
	}

	if stringutil.IsTrimmedEmpty(c.Endpoint) {
		err = errors.Join(err, errors.New("`telemetry.endpoint` is required"))
	}

	return err
}
