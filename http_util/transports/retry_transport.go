package transports

import (
	"context"
	"errors"
	"net"
	"net/http"
	"time"

	"go.jamie.mt/logx/log"
	"go.uber.org/zap"
)

// RetryFunc returning the response and possible error, if true is returned
// it'll retry given the maximum attempts aren't exceeded.
type RetryFunc func(*http.Response, error) bool

var TimeoutRetryFunc = func(r *http.Response, err error) bool {
	netErr, ok := err.(net.Error)
	return ok && netErr.Timeout()
}

var ErrMaximumRetryAttempts = errors.New("maximum retry attempts reached")

// RetryTransport is a custom transport that implements retry logic for HTTP requests.
// It wraps an existing http.RoundTripper implementation and retries failed requests up to a maximum count.
type RetryTransport struct {
	Transport     http.RoundTripper // The underlying HTTP transport
	RetryFunc     RetryFunc         // The retry function filter, if true is returned it'll retry
	RetryCount    int               // The maximum number of retry attempts
	RetryDuration time.Duration     // The duration to wait between retries
}

// L returns a logger instance associated with the RetryTransport.
// It provides a structured logging experience with trace information.
func (rt RetryTransport) L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named("retry_transport")
}

// RoundTrip implements the http.RoundTripper interface.
// It performs the HTTP request and retries failed requests up to the maximum retry count.
// If a timeout error occurs, it logs the error and waits for the specified retry duration before retrying.
// If a non-timeout error occurs, it immediately returns the error without retrying.
func (rt *RetryTransport) RoundTrip(req *http.Request) (res *http.Response, err error) {
	ctx := req.Context()
	for attempt := 1; attempt <= rt.RetryCount; attempt++ {
		res, err := rt.Transport.RoundTrip(req)
		if err == nil || attempt >= rt.RetryCount {
			return res, err
		} else if (rt.RetryFunc != nil && rt.RetryFunc(res, err)) || (rt.RetryFunc == nil && TimeoutRetryFunc(res, err)) {
			rt.L(req.Context()).Info(
				"failed but action is retryable",
				zap.Int("attempt", attempt),
				zap.Duration("sleep", rt.RetryDuration),
				zap.Error(err),
			)
			select {
			case <-ctx.Done():
				return res, err
			case <-time.After(rt.RetryDuration):
			}
		} else {
			return res, err
		}
	}
	return nil, ErrMaximumRetryAttempts
}

// Ensure that RetryTransport implements the http.RoundTripper interface.
var _ http.RoundTripper = &RetryTransport{}
