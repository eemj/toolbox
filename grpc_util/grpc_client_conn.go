package grpcutil

import (
	"context"

	"go.jamie.mt/toolbox/configmodels"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"google.golang.org/grpc"
)

type ClientConnConfig struct {
	// Config required that defines a client connection.
	// Validate isn't ran here, it assumes that the config has been
	// validated during the parsing phase.
	Config configmodels.GRPCClient

	// StreamInterceptors adds stream interceptors, they run at the very last
	// before the important interceptors are added.
	StreamInterceptors []grpc.StreamClientInterceptor

	// UnaryInterceptors adds unary interceptors, alike the stream interceptors
	// they run at the very last before the important interceptors are added.
	UnaryInterceptors []grpc.UnaryClientInterceptor

	// EnableTelemetry if true, it includes the opentelemetry interceptors
	// to both the unary and stream operations.
	EnableTelemetry bool

	// ExtraDialOptions provides the ability to specify extra grpc.DialOptions
	// that will be used when calling grpc.NewClient.
	ExtraDialOptions []grpc.DialOption
}

func NewClientConn(
	ctx context.Context,
	cfg ClientConnConfig,
) (grpc.ClientConnInterface, error) {
	transportCredentials, err := cfg.Config.Credentials.TransportCredentials()
	if err != nil {
		return nil, err
	}

	var (
		opts               []grpc.DialOption
		streamInterceptors []grpc.StreamClientInterceptor
		unaryInterceptors  []grpc.UnaryClientInterceptor
	)

	// EnableTelemetry adds the otelgrpc stat handler.
	if cfg.EnableTelemetry {
		otelGrpcStatHandler := otelgrpc.NewClientHandler()
		opts = append(opts, grpc.WithStatsHandler(otelGrpcStatHandler))
	}
	if len(cfg.ExtraDialOptions) > 0 {
		opts = append(opts, cfg.ExtraDialOptions...)
	}

	// We'll include the provided interceptors.
	if len(cfg.StreamInterceptors) > 0 {
		streamInterceptors = append(streamInterceptors, cfg.StreamInterceptors...)
	}
	if len(cfg.UnaryInterceptors) > 0 {
		unaryInterceptors = append(unaryInterceptors, cfg.UnaryInterceptors...)
	}

	opts = append(
		opts,
		grpc.WithTransportCredentials(transportCredentials),
		grpc.WithChainStreamInterceptor(streamInterceptors...),
		grpc.WithChainUnaryInterceptor(unaryInterceptors...),
	)

	return grpc.NewClient(
		cfg.Config.Address,
		opts...,
	)
}
