package configparser

import (
	"os"
	"path/filepath"
	"runtime"
	"slices"
)

// GetPaths returns the different paths based on the operating system running
// this method.
func GetPaths(applicationName string) ([]string, error) {
	wd, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	paths := []string{wd}
	parentPath, err := filepath.Abs(filepath.Join("../", applicationName))
	if err == nil {
		paths = append(paths, parentPath)
	}

	// configuration paths
	switch runtime.GOOS {
	case "windows":
		paths = append(
			paths,
			os.ExpandEnv(filepath.Join("$PROGRAMDATA", applicationName)),
			os.ExpandEnv(filepath.Join("$APPDATA", applicationName)),
			os.ExpandEnv(filepath.Join("$USERPROFILE", applicationName)),
		)
	case "darwin":
		paths = append(
			paths,
			os.ExpandEnv(filepath.Join("/Library", "Application Support", applicationName)),
			os.ExpandEnv(filepath.Join("$HOME", "/Library", "Application Support", applicationName)),
		)
	case "linux", "freebsd":
		paths = append(
			paths,
			os.ExpandEnv(filepath.Join("$XDC_CONFIG_HOME/", applicationName)),
			os.ExpandEnv(filepath.Join("$XDC_CONFIG_DIRS/", applicationName)),
			os.ExpandEnv(filepath.Join("/etc/", applicationName)),
			os.ExpandEnv(filepath.Join("$HOME/", applicationName)),
			os.ExpandEnv(filepath.Join("$HOME/.config", applicationName)),
		)
	}

	// Exclude executables
	paths = slices.DeleteFunc(paths, func(path string) bool {
		stat, _ := os.Stat(path)
		return stat != nil && !stat.IsDir()
	})

	return paths, nil
}
