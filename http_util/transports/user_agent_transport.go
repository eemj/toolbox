package transports

import (
	"net/http"

	"go.jamie.mt/toolbox/http_util/headers"
)

// UserAgentGenerator is a function type that returns a string representing a User-Agent value.
// Implementations of this function can be used to generate custom User-Agent strings.
// For example, you can use `github.com/corpix/uarand`.GetRandom().
type UserAgentGenerator func() string

// UserAgentTransport is a custom transport that sets a User-Agent header on outgoing HTTP requests.
// It wraps an existing http.RoundTripper implementation and adds the functionality to set a custom User-Agent.
type UserAgentTransport struct {
	Generator UserAgentGenerator // The User-Agent generator function
	Transport http.RoundTripper  // The underlying HTTP transport
}

// RoundTrip implements the http.RoundTripper interface.
// It sets the User-Agent header on the request using the provided Generator (if not nil),
// and delegates the actual request handling to the wrapped Transport.
// The response and error from the wrapped Transport.RoundTrip method are returned.
func (t *UserAgentTransport) RoundTrip(req *http.Request) (res *http.Response, err error) {
	if t.Generator != nil {
		req.Header.Set(headers.HeaderUserAgent, t.Generator())
	}

	return t.Transport.RoundTrip(req)
}

// Ensure that UserAgentTransport implements the http.RoundTripper interface.
var _ http.RoundTripper = &UserAgentTransport{}
