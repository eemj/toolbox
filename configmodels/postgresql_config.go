package configmodels

import (
	"errors"
	"strconv"
	"strings"

	stringutil "go.jamie.mt/toolbox/string_util"
)

type PostgreSQL struct {
	Hostname string `json:"hostname" mapstructure:"hostname"`
	Port     uint16 `json:"port" mapstructure:"port"`

	Username string `json:"username" mapstructure:"username"`
	Password string `json:"password" mapstructure:"password"`

	Name string `json:"name" mapstructure:"name"`

	Properties map[string]string `json:"properties" mapstructure:"properties"`
}

func (d PostgreSQL) string(safe bool) string {
	sb := new(strings.Builder)
	sb.WriteString("postgres://")

	if !stringutil.IsTrimmedEmpty(d.Username) && !stringutil.IsTrimmedEmpty(d.Password) {
		sb.WriteString(d.Username)
		sb.WriteRune(':')

		if safe {
			sb.WriteString("*****")
		} else {
			sb.WriteString(d.Password)
		}
		sb.WriteRune('@')
	}

	sb.WriteString(d.Hostname)
	sb.WriteRune(':')

	if d.Port == 0 {
		sb.WriteString("5432")
	} else {
		sb.WriteString(strconv.FormatUint(uint64(d.Port), 10))
	}

	sb.WriteRune('/')
	sb.WriteString(d.Name)

	index := 0

	for property, value := range d.Properties {
		if stringutil.IsTrimmedEmpty(property) || stringutil.IsTrimmedEmpty(value) {
			continue
		}

		if index == 0 {
			sb.WriteRune('?')
		} else {
			sb.WriteRune('&')
		}

		sb.WriteString(property)
		sb.WriteRune('=')
		sb.WriteString(value)

		index++
	}

	return sb.String()
}

func (d PostgreSQL) UnsafeString() string { return d.string(false) }
func (d PostgreSQL) String() string       { return d.string(true) }

func (d *PostgreSQL) Validate() (err error) {
	if d.Port == 0 { // Default to 5432 if it's empty.
		d.Port = 5432
	}

	if stringutil.IsTrimmedEmpty(d.Hostname) {
		err = errors.Join(err, errors.New("`database.hostname` is required"))
	}
	if stringutil.IsTrimmedEmpty(d.Name) {
		err = errors.Join(err, errors.New("`database.name` is required"))
	}
	if stringutil.IsTrimmedEmpty(d.Username) {
		err = errors.Join(err, errors.New("`database.username` is required"))
	}
	if stringutil.IsTrimmedEmpty(d.Password) {
		err = errors.Join(err, errors.New("`database.password` is required"))
	}

	return
}

var _ ConfigValidator = &PostgreSQL{}
