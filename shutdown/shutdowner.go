package shutdown

import (
	"context"
)

// Shutdowner defines the interface for something that can be gracefully shutdown,
// such as `*http.Server`.
type Shutdowner interface {
	Shutdown(ctx context.Context) (err error)
}

type noopShutdowner struct{}

// Shutdown implements Shutdowner
func (*noopShutdowner) Shutdown(context.Context) (err error) {
	return nil
}

// NewNoop defines a non operational shutdown.
func NewNoop() Shutdowner {
	return &noopShutdowner{}
}
