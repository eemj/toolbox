// Copyright (c) 2023 eemj
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

package saramautil

import (
	"context"

	"github.com/IBM/sarama"
	"go.opentelemetry.io/otel/trace"
)

const (
	headerTraceID string = "Sarama-Msg-TraceID"
	headerSpanID  string = "Sarama-Msg-SpanID"
)

// TraceAndSpanFromHeaders iterates through the headers and if one of the headers
// matches the expected TraceID and SpanID keys, it'll attempt to parse it's hex
// values.
func TraceAndSpanFromHeaders(
	headers ...*sarama.RecordHeader,
) (traceID trace.TraceID, spanID trace.SpanID, err error) {
	for _, header := range headers {
		switch string(header.Key) {
		case headerTraceID:
			traceID, err = trace.TraceIDFromHex(string(header.Value))
			if err != nil {
				return
			}
		case headerSpanID:
			spanID, err = trace.SpanIDFromHex(string(header.Value))
			if err != nil {
				return
			}
		default:
			continue
		}
		if traceID.IsValid() && spanID.IsValid() {
			break
		}
	}

	return traceID, spanID, nil
}

// NewProducerMessage retrieves the TraceID and SpanID from the provided context
// and if they're both valid it'll return a *sarama.ProducerMessage
// with pre-populated telemetry headers, otherwise it'll return an empty
// initialized *sarama.ProducerMessage.
func NewProducerMessage(ctx context.Context) *sarama.ProducerMessage {
	var (
		spanContext = trace.SpanContextFromContext(ctx)
		headers     []sarama.RecordHeader
	)

	if spanContext.IsValid() {
		headers = append(
			headers,
			sarama.RecordHeader{Key: []byte(headerSpanID), Value: []byte(spanContext.SpanID().String())},
			sarama.RecordHeader{Key: []byte(headerTraceID), Value: []byte(spanContext.TraceID().String())},
		)
	}

	return &sarama.ProducerMessage{Headers: headers}
}

// NewSpanContextFromConsumerMessage returns a an initialized SpanContext,
// where the TraceID and the SpanID are retrieved from the the consumer message
// headers.
func NewSpanContextFromConsumerMessage(message *sarama.ConsumerMessage) trace.SpanContext {
	traceID, spanID, err := TraceAndSpanFromHeaders(message.Headers...)
	contextConfig := trace.SpanContextConfig{
		TraceFlags: trace.FlagsSampled,
		Remote:     true,
	}
	if err == nil {
		contextConfig.TraceID = traceID
		contextConfig.SpanID = spanID
	}
	return trace.NewSpanContext(contextConfig)
}

// NewContextFromConsumerMessage returns a context with an initialized SpanContext,
// where the TraceID and the SpanID are retrieved from the the consumer message
// headers.
func NewContextFromConsumerMessage(ctx context.Context, message *sarama.ConsumerMessage) context.Context {
	spanContext := NewSpanContextFromConsumerMessage(message)
	if !spanContext.IsValid() {
		return ctx
	}
	return trace.ContextWithSpanContext(ctx, spanContext)
}
