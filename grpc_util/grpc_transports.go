package grpcutil

import (
	"net"

	recoverygrpc "github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors/recovery"
	"go.jamie.mt/toolbox/configmodels"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"google.golang.org/grpc"
)

// GRPCTransport represents a built grpc server along with the transport
// configuration used to build it.
type GRPCTransport struct {
	Server *grpc.Server
	Config configmodels.GRPCServerTransport
}

// Listener attempts to create a net.Listener.
func (s *GRPCTransport) Listener() (net.Listener, error) {
	return net.Listen(
		"tcp",
		s.Config.HostPort(),
	)
}

// TransportBuilderConfig defines the configuration used to build the transports.
type TransportBuilderConfig struct {
	// Config required to build the transports.
	// Validate isn't ran here, it assumes that the config has been
	// validated during the parsing phase.
	Config configmodels.GRPCServer
	// StreamInterceptors adds stream interceptors, they run at the very last
	// before the important interceptors are added.
	StreamInterceptors []grpc.StreamServerInterceptor
	// UnaryInterceptors adds unary interceptors, as the stream interceptor
	// they run at the very last before the important interceptors are added.
	UnaryInterceptors []grpc.UnaryServerInterceptor
	// EnableTelemetry if true, it includes opentelemetry interceptors
	// to both the unary and stream operations.
	EnableTelemetry bool
	// ServerOptions are hopefully, options which won't be used during the
	// transport building, such as a load balancer policy.
	ServerOptions []grpc.ServerOption
	// RecoveryOptions are options passed to the recoverer middleware
	RecoveryOptions []recoverygrpc.Option
	// OtelOptions are options passed to the otelgrpc middleware, only applicable
	// if EnableTelemetry is true
	OtelOptions []otelgrpc.Option
}

// NewTransports based on the provided configuration builds multiple transports
// no services are registered so it's important that one iterates through the
// transports once created and registers accordingly.
func NewTransports(cfg TransportBuilderConfig) (transports []*GRPCTransport, err error) {
	transports = make([]*GRPCTransport, len(cfg.Config.Transports))

	for index, transportConfig := range cfg.Config.Transports {
		credentials, err := transportConfig.Credentials.TransportCredentials()
		if err != nil {
			return nil, err
		}

		var (
			unaryInterceptors  []grpc.UnaryServerInterceptor
			streamInterceptors []grpc.StreamServerInterceptor
		)

		// EnableTelemetry adds the otelgrpc interceptor
		if cfg.EnableTelemetry {
			streamInterceptors = append(streamInterceptors, otelgrpc.StreamServerInterceptor(cfg.OtelOptions...))
			unaryInterceptors = append(unaryInterceptors, otelgrpc.UnaryServerInterceptor(cfg.OtelOptions...))
		}

		// We'll include the grpc interceptor
		if len(cfg.StreamInterceptors) > 0 {
			streamInterceptors = append(streamInterceptors, cfg.StreamInterceptors...)
		}
		if len(cfg.UnaryInterceptors) > 0 {
			unaryInterceptors = append(unaryInterceptors, cfg.UnaryInterceptors...)
		}

		// Recovery should literally be the 1st middleware that runs, therefore,
		// we need to make sure that it's the last added middleware
		// given the GRPC documentation specifies that the last chained interceptor
		// is the first one that runs.
		streamInterceptors = append(
			streamInterceptors,
			recoverygrpc.StreamServerInterceptor(cfg.RecoveryOptions...),
		)
		unaryInterceptors = append(
			unaryInterceptors,
			recoverygrpc.UnaryServerInterceptor(cfg.RecoveryOptions...),
		)

		serverOptions := []grpc.ServerOption{
			grpc.Creds(credentials),
			grpc.ChainUnaryInterceptor(unaryInterceptors...),
			grpc.ChainStreamInterceptor(streamInterceptors...),
		}
		if len(cfg.ServerOptions) > 0 {
			serverOptions = append(serverOptions, cfg.ServerOptions...)
		}

		transports[index] = &GRPCTransport{
			Server: grpc.NewServer(serverOptions...),
			Config: transportConfig,
		}
	}

	return transports, nil
}
