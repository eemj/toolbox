package sliceutil_test

import (
	"testing"

	sliceutil "go.jamie.mt/toolbox/slice_util"
	"go.uber.org/zap/zapcore"
)

func TestDistinct(t *testing.T) {
	var (
		letters = []string{
			"a", "a", "b", "c", "d", "d", "e",
		}
		items = sliceutil.Distinct(letters)
		count = make(map[string]int)
	)

	// Set a count for how many times we encountered an itemm
	// after it has been distinct
	for _, item := range items {
		if value, exists := count[item]; exists {
			count[item] = (value + 1)
		} else {
			count[item] = 1
		}
	}

	// Ensure that the count is 1 for all
	for key, value := range count {
		if value != 1 {
			t.Fatalf(
				"expected count to be 1 but got '%d' for key '%s'",
				value,
				key,
			)
		}
	}
}

func TestSurroundWith(t *testing.T) {
	result := sliceutil.SurroundWith(
		'\'',
		zapcore.DebugLevel.CapitalString(),
		zapcore.InfoLevel.CapitalString(),
		zapcore.WarnLevel.CapitalString(),
		zapcore.ErrorLevel.CapitalString(),
		zapcore.FatalLevel.CapitalString(),
		zapcore.DPanicLevel.CapitalString(),
	)

	if result != `'DEBUG', 'INFO', 'WARN', 'ERROR', 'FATAL', 'DPANIC'` {
		t.Fatalf("unexpected result `%s`", result)
	}
}
