package configmodels

import (
	"errors"
	"time"

	stringutil "go.jamie.mt/toolbox/string_util"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
)

type GRPCClient struct {
	Address     string                 `json:"address" mapstructure:"address"`
	Timeout     time.Duration          `json:"timeout" mapstructure:"timeout"`
	Credentials *GRPCClientCredentials `json:"credentials" mapstructure:"credentials"`
}

func (c *GRPCClient) Validate() (err error) {
	if c.Timeout < 0 {
		err = errors.Join(err, errors.New("`client.timeout` must be greater than 0"))
	}

	if c.Timeout == 0 {
		c.Timeout = (500 * time.Millisecond)
	}

	if stringutil.IsTrimmedEmpty(c.Address) {
		err = errors.Join(err, errors.New("`client.address` is required"))
	}

	if c.Credentials == nil {
		err = errors.Join(err, errors.New("`client.credentials` is required"))
	} else {
		err = errors.Join(err, c.Credentials.Validate())
	}

	return err
}

type GRPCClientCredentials struct {
	Secure             bool   `json:"secure" mapstructure:"secure"`
	CertFile           string `json:"certFile" mapstructure:"cert_file"`
	ServerNameOverride string `json:"serverNameOverride" mapstructure:"server_name_override"`
}

func (c GRPCClientCredentials) TransportCredentials() (credentials.TransportCredentials, error) {
	if c.Secure {
		return credentials.NewClientTLSFromFile(c.CertFile, c.ServerNameOverride)
	}
	return insecure.NewCredentials(), nil
}

func (c GRPCClientCredentials) Validate() (err error) {
	if c.Secure {
		if stringutil.IsTrimmedEmpty(c.CertFile) {
			err = errors.Join(err, errors.New("`grpc.credentials.cert_file` is required"))
		}
	}

	return err
}
