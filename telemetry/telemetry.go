package telemetry // import "go.jamie.mt/toolbox/telemetry"

import (
	"context"

	"go.jamie.mt/toolbox/configmodels"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/propagation"

	"go.opentelemetry.io/otel/sdk/resource"
	"go.opentelemetry.io/otel/sdk/trace"
)

// SetupTelemetryProvider creates a telemetry provider with the following resources
// FromEnv, TelemetrySDK, Host(), ProcessExecutablePath(), ProcessOwner(), ProcessPID(),
// and custom defined attributes passed.
func SetupTelemetryProvider(
	ctx context.Context,
	exporter trace.SpanExporter,
	sampler trace.Sampler,
	attributes ...attribute.KeyValue,
) (*trace.TracerProvider, error) {
	resource, err := resource.New(
		ctx,
		resource.WithFromEnv(),
		resource.WithTelemetrySDK(),
		resource.WithHost(),
		resource.WithProcessExecutablePath(),
		resource.WithProcessOwner(),
		resource.WithProcessPID(),
		resource.WithAttributes(attributes...),
	)
	if err != nil {
		return nil, err
	}

	provider := trace.NewTracerProvider(
		trace.WithSampler(sampler),
		trace.WithBatcher(exporter),
		trace.WithResource(resource),
	)

	// Set this propogator globally.
	otel.SetTextMapPropagator(propagation.NewCompositeTextMapPropagator(
		propagation.TraceContext{},
		propagation.Baggage{},
	))

	// Set this provider globally
	otel.SetTracerProvider(provider)

	return provider, nil
}

// NewTraceProvider creates the actual exporter based on the config, certain options
// are only supported with an insecure option, if you have a pre-defined exporter
// use SetupTelemetryProvider instead.
func NewTraceProvider(
	ctx context.Context,
	cfg configmodels.Telemetry,
	attributes ...attribute.KeyValue,
) (*trace.TracerProvider, error) {
	if !cfg.Enable {
		return nil, nil
	}

	exporter, err := CreateExporter(cfg)
	if err != nil {
		return nil, err
	}

	sampler, err := cfg.Sampler.Sampler()
	if err != nil {
		return nil, err
	}

	return SetupTelemetryProvider(
		ctx,
		exporter,
		sampler,
		attributes...,
	)
}
