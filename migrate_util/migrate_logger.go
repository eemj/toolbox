package gomigrate

import (
	"strings"

	"github.com/golang-migrate/migrate"
	"go.uber.org/zap"
)

type migrationLogger struct {
	logger *zap.Logger
}

func (m *migrationLogger) Printf(format string, v ...interface{}) {
	m.logger.
		Sugar().
		Infof(strings.TrimSuffix(format, "\n"), v...)
}

func (*migrationLogger) Verbose() bool { return true }

func NewLogger(
	logger *zap.Logger,
) migrate.Logger {
	return &migrationLogger{
		logger: logger,
	}
}
