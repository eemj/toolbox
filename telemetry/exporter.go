package telemetry

import (
	"context"
	"fmt"

	"go.jamie.mt/toolbox/configmodels"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
	"go.opentelemetry.io/otel/exporters/zipkin"
	tracesdk "go.opentelemetry.io/otel/sdk/trace"
)

// CreateExporter returns an exporter based on the configmodels data.
func CreateExporter(cfg configmodels.Telemetry) (exporter tracesdk.SpanExporter, err error) {
	switch cfg.Kind {
	case configmodels.TelemetryKindZipkin:
		return zipkin.New(cfg.Endpoint)
	case configmodels.TelemetryKindOTLPGRPC:
		return otlptracegrpc.New(
			context.Background(),
			otlptracegrpc.WithInsecure(),
			otlptracegrpc.WithEndpoint(cfg.Endpoint),
		)
	case configmodels.TelemetryKindOTLPHTTP:
		return otlptracehttp.New(
			context.Background(),
			otlptracehttp.WithInsecure(),
			otlptracehttp.WithEndpoint(cfg.Endpoint),
		)
	}

	return nil, fmt.Errorf("unknown exporter: `%s`", cfg.Kind)
}
