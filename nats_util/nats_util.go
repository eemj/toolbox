package natsutil

import (
	"context"

	"github.com/nats-io/nats.go"
	"go.opentelemetry.io/otel/trace"
)

// Sourced from:
// https://stackoverflow.com/questions/73935038/how-to-pass-remote-parent-span-properly-using-nats

const (
	headerTraceID string = "Nats-Msg-TraceID"
	headerSpanID  string = "Nats-Msg-SpanID"
)

// NewTraceMsg is a wrapper around nats.NewMsg() which essentially
// attempts to get the span context from the given context
// and adds the trace ID and span ID to the NATS message.
func NewTraceMsg(ctx context.Context, subject string) *nats.Msg {
	spanContext := trace.SpanContextFromContext(ctx)
	msg := nats.NewMsg(subject)

	if spanContext.IsValid() {
		msg.Header.Set(headerTraceID, spanContext.TraceID().String())
		msg.Header.Set(headerSpanID, spanContext.SpanID().String())
	}

	return msg
}

// GetTraceContextFromMsg attempts to create a span context from the provided
// NATS message by reading the trace ID and the span ID from its headers.
func GetTraceContextFromMsg(msg *nats.Msg) (spanContext trace.SpanContext, err error) {
	traceID, err := trace.TraceIDFromHex(msg.Header.Get(headerTraceID))
	if err != nil {
		return spanContext, err
	}
	spanID, err := trace.SpanIDFromHex(msg.Header.Get(headerSpanID))
	if err != nil {
		return spanContext, err
	}
	return trace.NewSpanContext(trace.SpanContextConfig{
		TraceID:    traceID,
		SpanID:     spanID,
		TraceFlags: trace.FlagsSampled,
		Remote:     true,
	}), nil
}
