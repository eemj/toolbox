package healthserver

import (
	"context"
	"strings"
	"sync"
	"time"

	"google.golang.org/grpc/health/grpc_health_v1"
)

// ServiceConfig defines a health checkable service
// it's important to only list services here that you care about
// services such as a redis instance (given you have a circuit breaker)
// shouldn't be listed here.
type ServiceConfig struct {
	Name    string  `json:"name"`
	Service Service `json:"service"`
}

// Service defines a service that has the potential to be a part of the
// healthchecker, anything that respects this interface can be included
// as part of the healthchecker
type Service interface {
	Ping(ctx context.Context) (err error)
}

// healthServer implements grpc_health_v1.HealthServer
type healthServer struct {
	serviceRmu sync.RWMutex
	services   map[string]Service
}

// getStatus iterates through the registered services, regardless of case-sensitivity it attempts to find the service
// if the server is found it calls a ping, otherwise if there's no server it returns SERVICE_UNKNOWN.
// if the requester provided an empty string or a nil body, we'll consider it as a ping all operation.
func (hs *healthServer) getHealthResponse(ctx context.Context, req *grpc_health_v1.HealthCheckRequest) *grpc_health_v1.HealthCheckResponse {
	hs.serviceRmu.RLock()
	defer hs.serviceRmu.RUnlock()

	var (
		requestedName string
		status        = grpc_health_v1.HealthCheckResponse_SERVICE_UNKNOWN
	)
	if req != nil {
		requestedName = strings.ToUpper(req.Service)
	}

	if requestedName == "" {
		for _, service := range hs.services {
			if service.Ping(ctx) != nil {
				status = grpc_health_v1.HealthCheckResponse_NOT_SERVING
				break
			}
		}
		if status != grpc_health_v1.HealthCheckResponse_NOT_SERVING {
			status = grpc_health_v1.HealthCheckResponse_SERVING
		}
	} else {
		service, exists := hs.services[requestedName]
		if exists {
			if service.Ping(ctx) != nil {
				status = grpc_health_v1.HealthCheckResponse_NOT_SERVING
			} else {
				status = grpc_health_v1.HealthCheckResponse_SERVING
			}
		}
	}

	return &grpc_health_v1.HealthCheckResponse{Status: status}
}

// Check implements grpc_health_v1.HealthServer
func (hs *healthServer) Check(ctx context.Context, req *grpc_health_v1.HealthCheckRequest) (*grpc_health_v1.HealthCheckResponse, error) {
	return hs.getHealthResponse(ctx, req), nil
}

// Watch implements grpc_health_v1.HealthServer
func (hs *healthServer) Watch(req *grpc_health_v1.HealthCheckRequest, srv grpc_health_v1.Health_WatchServer) error {
	var (
		ctx            = srv.Context()
		checkResponse  = hs.getHealthResponse(ctx, req)
		previousStatus = checkResponse.Status
	)

	err := srv.Send(checkResponse)
	if err != nil {
		return err
	}

	if previousStatus == grpc_health_v1.HealthCheckResponse_SERVICE_UNKNOWN {
		return nil
	}

	ticker := time.NewTicker(5 * time.Second)
	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-ticker.C:
			checkResponse = hs.getHealthResponse(ctx, req)

			if previousStatus != checkResponse.Status {
				if err = srv.Send(checkResponse); err != nil {
					return err
				}
			}
		}
	}
}

func NewServer(cfgs ...ServiceConfig) grpc_health_v1.HealthServer {
	srv := &healthServer{services: make(map[string]Service)}
	for _, cfg := range cfgs {
		name := strings.ToUpper(cfg.Name)
		srv.services[name] = cfg.Service
	}
	return srv
}
