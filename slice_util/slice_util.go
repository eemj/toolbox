package sliceutil // import "go.jamie.mt/toolbox/slice_util"

import (
	"fmt"
	"strings"
)

// SurroundWith will separate elements by comma, the surroundChar is used to specify the
// type of quote or any other character they would like to use.
func SurroundWith[T any](surroundChar rune, params ...T) string {
	count := len(params)

	if count == 0 {
		return ""
	}

	var sb strings.Builder

	sb.WriteRune(surroundChar)

	for index, param := range params {
		sb.WriteString(fmt.Sprintf("%v", param))

		if (count - 1) != index {
			sb.WriteRune(surroundChar)
			sb.WriteString(", ")
			sb.WriteRune(surroundChar)
		}
	}

	sb.WriteRune(surroundChar)

	return sb.String()
}

// Distinct internally creates a set and compares does a comparison based on
// whatever was added to the set.
func Distinct[T comparable](items []T) []T {
	var (
		set    = make(map[T]struct{})
		result []T
	)
	for _, item := range items {
		if _, exists := set[item]; !exists {
			set[item] = struct{}{}
			result = append(result, item)
		}
	}
	return result
}

// SliceToSet converts a slice to a set of map[T]struct{}, if you have duplicate items
// the iterator will only add to the set the first match, leaving the others ignored.
func SliceToSet[T comparable](items []T) map[T]struct{} {
	set := make(map[T]struct{})
	for _, item := range items {
		if _, exists := set[item]; !exists {
			set[item] = struct{}{}
		}
	}
	return set
}
