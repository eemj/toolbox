package configmodels

import (
	"errors"
	"fmt"

	"github.com/nats-io/nats.go"
	sliceutil "go.jamie.mt/toolbox/slice_util"
	stringutil "go.jamie.mt/toolbox/string_util"
)

type NATSQueue struct {
	RetentionPolicy string `mapstructure:"retention_policy"`
}

func (q *NATSQueue) Validate() (err error) {
	if stringutil.IsTrimmedEmpty(q.RetentionPolicy) {
		err = errors.Join(err, errors.New("`nats.queue.retention_policy` is required"))
	}

	switch q.RetentionPolicy {
	case nats.WorkQueuePolicy.String(),
		nats.LimitsPolicy.String(),
		nats.InterestPolicy.String():
		break
	default:
		err = errors.Join(err, fmt.Errorf("`nats.queue.retention_policy` is invalid, expected one of: %v",
			sliceutil.SurroundWith(
				'\'',
				nats.WorkQueuePolicy.String(),
				nats.LimitsPolicy.String(),
				nats.InterestPolicy.String(),
			)),
		)
	}

	return
}
