package configmodels

type ConfigValidator interface {
	Validate() (err error)
}
