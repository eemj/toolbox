package configmodels

import (
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/IBM/sarama"
	sliceutil "go.jamie.mt/toolbox/slice_util"
	stringutil "go.jamie.mt/toolbox/string_util"
)

var _defaultSarama = sarama.NewConfig()

type KafkaOffset string

const (
	KafkaOffsetNewest KafkaOffset = "newest"
	KafkaOffsetOldest KafkaOffset = "oldest"
)

func (k KafkaOffset) String() string { return string(k) }

func (k KafkaOffset) Validate() error {
	switch k {
	case KafkaOffsetNewest, KafkaOffsetOldest:
		return nil
	}
	_, err := strconv.ParseInt(string(k), 10, 64)
	if err != nil {
		return fmt.Errorf("`offset` parse error: %w", err)
	}
	return nil
}

func (k KafkaOffset) Value() int64 {
	switch k {
	case KafkaOffsetNewest:
		return sarama.OffsetNewest
	case KafkaOffsetOldest:
		return sarama.OffsetOldest
	default:
		offset, err := strconv.ParseInt(string(k), 10, 64)
		if err == nil {
			return sarama.OffsetNewest
		}
		return offset
	}
}

type KafkaRebalanceStrategy string

const (
	KafkaRebalanceStrategyRange      KafkaRebalanceStrategy = "range"
	KafkaRebalanceStrategySticky     KafkaRebalanceStrategy = "sticky"
	KafkaRebalanceStrategyRoundRobin KafkaRebalanceStrategy = "roundrobin"
)

func (k KafkaRebalanceStrategy) String() string { return string(k) }

func (k KafkaRebalanceStrategy) Validate() error {
	switch k {
	case KafkaRebalanceStrategyRoundRobin,
		KafkaRebalanceStrategySticky,
		KafkaRebalanceStrategyRange:
		return nil
	}
	return fmt.Errorf(
		"`%s` unknown rebalance stategy, expected one of [%s]",
		k,
		sliceutil.SurroundWith('`', []KafkaRebalanceStrategy{
			KafkaRebalanceStrategyRange,
			KafkaRebalanceStrategySticky,
			KafkaRebalanceStrategyRoundRobin,
		}),
	)
}

func (k KafkaRebalanceStrategy) Value() sarama.BalanceStrategy {
	switch k {
	case KafkaRebalanceStrategyRange:
		return sarama.NewBalanceStrategyRange()
	case KafkaRebalanceStrategySticky:
		return sarama.NewBalanceStrategySticky()
	case KafkaRebalanceStrategyRoundRobin:
		return sarama.NewBalanceStrategyRoundRobin()
	}
	return sarama.NewBalanceStrategyRange() // Fallback to default in the sarama.NewConfig()
}

type Kafka struct {
	Name     string                `mapstructure:"name"`
	Topic    string                `mapstructure:"topic"`
	Producer *KafkaProducerOptions `mapstructure:"producer,omitempty"`
	Consumer *KafkaConsumerOptions `mapstructure:"consumer,omitempty"`
	Brokers  []string              `mapstructure:"brokers"`
	RackID   string                `mapstructure:"rack_id"`
}

type KafkaProducerOptions struct {
	Timeout         time.Duration `mapstructure:"timeout"`
	BackoffDuration time.Duration `mapstructure:"backoff_duration"`
	RetryMaximum    int           `mapstructure:"retry_maximum"`
	MaxMessageBytes int           `mapstructure:"max_message_bytes"`
}

func (k *KafkaProducerOptions) Validate() (err error) {
	if k.Timeout < 0 {
		err = errors.Join(err, errors.New("`producer.timeout` must be greater than 0"))
	}
	if k.BackoffDuration < 0 {
		err = errors.Join(err, errors.New("`producer.backoff_duration` must be greater than 0"))
	}
	if k.RetryMaximum < 0 {
		err = errors.Join(err, errors.New("`producer.retry_maximum` must be greater than 0"))
	}
	if k.MaxMessageBytes < 0 {
		err = errors.Join(err, errors.New("`producer.max_message_bytes` must be greater than 0"))
	}

	return err
}

func (k *KafkaProducerOptions) Assign(cfg *sarama.Config) {
	if cfg == nil {
		return
	}

	if cfg.Producer.Timeout > 0 {
		cfg.Producer.Timeout = k.Timeout
	}
	if k.BackoffDuration > 0 {
		cfg.Producer.Retry.Backoff = k.BackoffDuration
	}
	if k.RetryMaximum > 0 {
		cfg.Producer.Retry.Max = k.RetryMaximum
	}
	if k.MaxMessageBytes > 0 {
		cfg.Producer.MaxMessageBytes = k.MaxMessageBytes
	}
}

type KafkaConsumerOptions struct {
	RebalanceStrategy *KafkaRebalanceStrategy `mapstructure:"rebalance_strategy,omitempty"`
	Offset            *KafkaOffset            `mapstructure:"offset,omitempty"`
	GroupID           string                  `mapstructure:"group_id"`
	BackoffDuration   time.Duration           `mapstructure:"backoff_duration"`
}

func (k *KafkaConsumerOptions) Validate() (err error) {
	if stringutil.IsTrimmedEmpty(k.GroupID) {
		err = errors.Join(err, errors.New("`consumer.group_id` is required"))
	}
	if k.BackoffDuration < 0 {
		err = errors.Join(err, errors.New("`consumer.backoff_duration` must be greater than 0"))
	}
	if k.RebalanceStrategy != nil {
		err = errors.Join(err, k.RebalanceStrategy.Validate())
	}
	if k.Offset != nil {
		err = errors.Join(err, k.Offset.Validate())
	}

	return err
}

func (k *KafkaConsumerOptions) Assign(cfg *sarama.Config) {
	if cfg == nil {
		return
	}

	if k.Offset != nil {
		cfg.Consumer.Offsets.Initial = k.Offset.Value()
	}
	if k.RebalanceStrategy != nil {
		cfg.Consumer.Group.Rebalance.GroupStrategies = []sarama.BalanceStrategy{
			k.RebalanceStrategy.Value(),
		}
	}
	if k.BackoffDuration > 0 {
		cfg.Consumer.Retry.Backoff = k.BackoffDuration
	}
}

func (k *Kafka) Validate() (err error) {
	if len(k.Brokers) == 0 {
		err = errors.Join(err, errors.New("`brokers` is required and must not be empty"))
	}
	if stringutil.IsTrimmedEmpty(k.Name) {
		err = errors.Join(err, errors.New("`name` is required"))
	}
	if stringutil.IsTrimmedEmpty(k.Topic) {
		err = errors.Join(err, errors.New("`topic` is required"))
	}
	if k.Consumer == nil && k.Producer == nil {
		err = errors.Join(err, errors.New("`consumer` or `producer` must be specified"))
	}
	if k.Consumer != nil {
		err = errors.Join(err, k.Consumer.Validate())
	}
	if k.Producer != nil {
		err = errors.Join(err, k.Producer.Validate())
	}

	return err
}
