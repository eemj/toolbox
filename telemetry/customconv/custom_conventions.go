package customconv // import "go.jamie.mt/toolbox/telemetry/customconv"

import (
	"go.opentelemetry.io/otel/attribute"
)

const (
	// DBHostKey returns `db.host`
	DBHostKey = attribute.Key(`db.host`)
	// DBPortKey returns `db.port`
	DBPortKey = attribute.Key(`db.port`)
	// DBUserKey returns `db.user`
	DBUserKey = attribute.Key(`db.user`)
	// DBNameKey returns `db.name`
	DBNameKey = attribute.Key(`db.name`)
	// DBStatementArgumentsKey returns `db.statement.arguments`
	DBStatementArgumentsKey = attribute.Key(`db.statement.arguments`)

	// ServiceNameKey returns `service.name`
	ServiceNameKey = attribute.Key(`service.name`)

	// NATSSubjectKey returns `nats.subject`
	NATSSubjectKey = attribute.Key(`nats.subject`)

	// AWSS3ObjectKey returns `aws.s3.object_key`
	AWSS3ObjectKey = attribute.Key(`aws.s3.object_key`)

	// RateLimitKey returns `rate_limit.key`
	RateLimitKeyKey = attribute.Key(`rate_limit.key`)
	// RateLimitResetAtKey returns `rate_limit.reset_at`
	RateLimitResetAtKey = attribute.Key(`rate_limit.reset_at`)
	// RateLimitKeyKey returns `rate_limit.limit`
	RateLimitLimitKey = attribute.Key(`rate_limit.limit`)
	// RateLimitRemainingKey returns `rate_limit.remaining`
	RateLimitRemainingKey = attribute.Key(`rate_limit.remaining`)
	// RateLimitReachedKey returns `rate_limit.reached`
	RateLimitReachedKey = attribute.Key(`rate_limit.reached`)
)
