package configmodels

import (
	"fmt"
	"strings"

	sliceutil "go.jamie.mt/toolbox/slice_util"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

type Logging struct {
	Level string      `mapstructure:"level" json:"level"`
	File  FileLogging `mapstructure:"file" json:"file"`
}

type FileLogging struct {
	Enable     bool   `mapstructure:"enable" json:"enable"`
	Compress   bool   `mapstructure:"compress" json:"compress"`
	MaxBackups int    `mapstructure:"max_backups" json:"maxBackups"`
	MaxSizeMB  int    `mapstructure:"max_size_mb" json:"maxSizeMB"`
	MaxAgeDays int    `mapstructure:"max_age_days" json:"maxAgeDays"`
	Path       string `mapstructure:"path" json:"path"`
}

func (c *Logging) Validate() (err error) {
	level := strings.ToUpper(strings.TrimSpace(c.Level))

	switch level {
	case zap.InfoLevel.CapitalString(),
		zap.DebugLevel.CapitalString(),
		zap.WarnLevel.CapitalString(),
		zap.ErrorLevel.CapitalString(),
		zap.FatalLevel.CapitalString(),
		zap.DPanicLevel.CapitalString():
	default:
		return fmt.Errorf(
			"unexpected `level`, expected one of [%v]",
			sliceutil.SurroundWith('`',
				zap.DebugLevel.CapitalString(),
				zap.InfoLevel.CapitalString(),
				zap.WarnLevel.CapitalString(),
				zap.ErrorLevel.CapitalString(),
				zap.FatalLevel.CapitalString(),
				zap.DPanicLevel.CapitalString(),
			),
		)
	}

	return nil
}

func (c *Logging) ZapWriteSync() zapcore.WriteSyncer {
	if !c.File.Enable {
		return nil
	}

	return zapcore.AddSync(&lumberjack.Logger{
		Filename:   c.File.Path,
		Compress:   c.File.Compress,
		MaxSize:    c.File.MaxSizeMB,
		MaxAge:     c.File.MaxAgeDays,
		MaxBackups: c.File.MaxBackups,
	})
}

func (c *Logging) ZapLevel() zapcore.Level {
	switch strings.ToUpper(c.Level) {
	case zap.InfoLevel.CapitalString():
		return zap.InfoLevel
	case zap.DebugLevel.CapitalString():
		return zap.DebugLevel
	case zap.ErrorLevel.CapitalString():
		return zap.ErrorLevel
	case zap.FatalLevel.CapitalString():
		return zap.FatalLevel
	case zap.WarnLevel.CapitalString():
		return zap.WarnLevel
	case zap.DPanicLevel.CapitalString():
		return zap.DPanicLevel
	}

	return zap.InfoLevel
}
