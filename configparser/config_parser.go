package configparser

import (
	"os"
	"path/filepath"
	"strings"

	"github.com/knadh/koanf/parsers/json"
	"github.com/knadh/koanf/providers/file"
	"github.com/knadh/koanf/v2"
	"go.jamie.mt/toolbox/configparser/providers/envtojson"
)

// Parsable represents a config that can be validated and a path to be set.
type Parsable interface {
	// Validate should return when there's a validation error.
	Validate() (err error)
	// SetPath will be called with the config loaded path.
	SetPath(path string)
}

// Parse attempts to parse the configuration in JSON format from paths from GetPaths
// afterwards, environment variables override the config values.
func Parse[C Parsable](opts ParserOptions) (c C, err error) {
	konf := koanf.New(opts.ConfigDelimiter)

	paths, err := GetPaths(opts.ApplicationName)
	if err != nil {
		return c, err
	}

	var configPathUsed *string

	for _, path := range paths {
		configPath := filepath.Join(path, opts.Filename)

		err := konf.Load(file.Provider(configPath), json.Parser())
		if err != nil {
			if os.IsNotExist(err) {
				continue
			}
			return c, err
		}

		configPathUsed = new(string)
		*configPathUsed = configPath
		break
	}

	environmentKeyPrefix := opts.EnvironmentReplacer + opts.EnvironmentDelimiter

	err = konf.Load(
		envtojson.Provider(
			opts.EnvironmentReplacer,
			opts.EnvironmentDelimiter,
			func(s string) string {
				return strings.TrimPrefix(s, environmentKeyPrefix)
			},
		),
		json.Parser(),
	)
	if err != nil {
		return c, err
	}

	if err := konf.UnmarshalWithConf("", &c, koanf.UnmarshalConf{
		Tag: `mapstructure`,
	}); err != nil {
		return c, err
	}

	if configPathUsed != nil {
		c.SetPath(*configPathUsed)
	}

	if err = c.Validate(); err != nil {
		return
	}

	return c, nil
}
