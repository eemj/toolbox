package configmodels

import (
	"errors"
	"net"
	"strconv"

	stringutil "go.jamie.mt/toolbox/string_util"
)

type GRPCGatewayConfig struct {
	Client GRPCClient `json:"client" mapstructure:"client"`
	Host   string     `json:"host" mapstructure:"host"`
	Port   int        `json:"port" mapstructure:"port"`
}

func (c *GRPCGatewayConfig) HostPort() string {
	return net.JoinHostPort(
		c.Host,
		strconv.FormatInt(int64(c.Port), 10),
	)
}

func (g *GRPCGatewayConfig) Validate() (err error) {
	if stringutil.IsTrimmedEmpty(g.Host) {
		g.Host = "0.0.0.0"
	}

	if g.Port < 0 || g.Port > 65535 {
		err = errors.Join(err, errors.New("`grpc.port` must be between 0 and 65535"))
	}

	err = errors.Join(err, g.Client.Validate())

	return err
}
