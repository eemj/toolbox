package stringutil // import "go.jamie.mt/toolbox/string_util"

import "strings"

// IsTrimmedEmpty returns if a string with spaces is empty or not.
// example 1: ' x' -> false
// example 2: ' ' -> true
func IsTrimmedEmpty(str string) bool {
	return len(strings.TrimSpace(str)) == 0
}
