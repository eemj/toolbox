package transports

import (
	"context"
	"net/http"
	"time"

	"github.com/ulule/limiter/v3"
	"go.jamie.mt/logx/log"
	"go.jamie.mt/toolbox/telemetry/customconv"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

// RateLimitAlign is a function that's used after a response is retrieved to align the rate limit.
// It takes the request context and the HTTP response as parameters and returns the actual rate limit values.
// If the alignment is successful, it returns the rate limit, remaining limit, and a boolean flag indicating success.
// If the alignment is not possible or the response does not contain the rate limit information,
// the function should return zero values for the rate limit and remaining limit, and false for the success flag.
// This function can be used to handle cases where the API returns a status code other than 429 (Too Many Requests)
// but the rate limit has been reached such as GraphQL.
type RateLimitAlign func(ctx context.Context, res *http.Response) (limit, remaining int64, ok bool)

// RateLimitTransport is a custom transport that handles rate limiting of HTTP requests.
// It wraps an existing http.RoundTripper implementation and enforces rate limits using a limiter.
type RateLimitTransport struct {
	Transport  http.RoundTripper // The underlying HTTP transport
	Align      RateLimitAlign    // The rate limit align function
	Limiter    *limiter.Limiter  // The rate limiter
	LimiterKey string            // The key used to identify the rate limiter
}

// L returns a logger instance associated with the RateLimitTransport.
// It provides a structured logging experience with trace information.
func (rt RateLimitTransport) L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named("rate_limit_transport")
}

// RoundTrip implements the http.RoundTripper interface.
// It performs the HTTP request and enforces rate limits using the configured limiter.
// If the rate limit is reached, it waits for the duration until the rate limit resets before retrying the request.
// If the response status code is 429 (Too Many Requests), it increments the rate limit count by the limit value.
// If an alignment function is provided, it adjusts the rate limit based on the actual values returned by the alignment function.
func (rt *RateLimitTransport) RoundTrip(req *http.Request) (res *http.Response, err error) {
	ctx := req.Context()

	limit, err := rt.Limiter.Increment(ctx, rt.LimiterKey, 1)
	if err != nil {
		return nil, err
	}

	span := trace.SpanFromContext(ctx)

	if span != nil && span.IsRecording() {
		span.AddEvent("rate_limit.update", rt.rateLimitAttributes(limit))
	}

	if limit.Reached {
		duration := time.Until(time.Unix(limit.Reset, 0))

		rt.L(ctx).Debug(
			"sleeping",
			zap.Duration("duration", duration),
			zap.String("key", rt.LimiterKey),
		)

		select {
		case <-ctx.Done():
			return nil, ctx.Err()
		case <-time.After(duration):
			break
		}
	}

	res, err = rt.Transport.RoundTrip(req)
	if err != nil {
		return nil, err
	}

	if res.StatusCode == http.StatusTooManyRequests {
		if span != nil && span.IsRecording() {
			span.AddEvent("rate_limit.burst")
		}

		rt.L(ctx).Debug("rate_limit, burst")

		limit, err = rt.Limiter.Increment(ctx, rt.LimiterKey, limit.Limit)
		if err != nil {
			return nil, err
		}
	} else {
		err := rt.alignLimits(ctx, span, limit, res)
		if err != nil {
			return nil, err
		}
	}

	return res, nil
}

// alignLimits adjusts the rate limit based on the actual values returned by the alignment function.
func (rt *RateLimitTransport) alignLimits(
	ctx context.Context,
	span trace.Span,
	limit limiter.Context,
	res *http.Response,
) error {
	if rt.Align == nil {
		return nil
	}

	currentLimit, _, ok := rt.Align(ctx, res)
	remainingLimit := limit.Limit - limit.Remaining

	if ok && currentLimit > 0 && currentLimit > remainingLimit {
		incrementBy := currentLimit - remainingLimit

		rt.L(ctx).Debug(
			"aligning and incrementing",
			zap.Int64("distributed_limit", remainingLimit),
			zap.Int64("current_limit", currentLimit),
			zap.Int64("increment_by", incrementBy),
		)

		limit, err := rt.Limiter.Increment(ctx, rt.LimiterKey, incrementBy)
		if err != nil {
			return err
		}

		span.AddEvent("rate_limit.adjustment", rt.rateLimitAttributes(limit))
	}

	return nil
}

// rateLimitAttributes returns the rate limit attributes as trace span options.
func (rt RateLimitTransport) rateLimitAttributes(ctx limiter.Context) trace.SpanStartEventOption {
	return trace.WithAttributes(
		customconv.RateLimitKeyKey.String(rt.LimiterKey),
		customconv.RateLimitResetAtKey.Int64(ctx.Reset),
		customconv.RateLimitLimitKey.Int64(ctx.Limit),
		customconv.RateLimitRemainingKey.Int64(ctx.Remaining),
		customconv.RateLimitReachedKey.Bool(ctx.Reached),
	)
}

// Ensure that RateLimitTransport implements the http.RoundTripper interface.
var _ http.RoundTripper = &RateLimitTransport{}
