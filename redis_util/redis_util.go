package redisutil

import (
	"context"
)

type LoggerFunc func(ctx context.Context, format string, v ...interface{})

type Logger struct {
	fn LoggerFunc
}

func (r *Logger) Printf(ctx context.Context, format string, v ...interface{}) {
	r.fn(ctx, format, v...)
}

func NewLogger(fn LoggerFunc) *Logger {
	return &Logger{fn: fn}
}
